const navClicked = () => {
	const menu = document.querySelector('.menuBurger');
	const navLinks = document.querySelector('.nav-links');
	const links = document.querySelectorAll('.nav-links li');
	
	menu.addEventListener('click', () => {
		navLinks.classList.toggle('nav-clicked')
		links.forEach((link, index) => {
			link.style.animation ? link.style.animation = '' : link.style.animation = `navLinks 0.5s ease forwards ${index / 7 + 0.3}s`	
		})
	})
}

navClicked();