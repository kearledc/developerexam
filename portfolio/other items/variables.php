<!-- #2 -->

<!-- #3 -->
<?php
	DATA : [ 1 , -1 ,  3 , -4 ,  5 , -2 ,  7 ,  4 ,  2 ]
	RETURN : [ 1 , 2 , 4 ]

?>

<!-- JS -->


<!-- #4 -->

CREATE player_DB;

use player_DB;

CREATE TABLE player (
	player_id INT PRIMARY KEY,
	name VARCHAR(255)
)

CREATE TABLE game (
	game_id INT PRIMARY KEY,
	name VARCHAR(255)
);

CREATE TABLE score (
	score_id INT PRIMARY KEY,
	player_id INT
	FOREIGN KEY(player_id)
	REFERENCES player(id),
	game_id INT
	FOREIGN_KEY(game_id)
	REFERENCES game(id),
	score INT
	ON UPDATE CASCADE
	ON DELETE SET NULL
);

CREATE TABLE leaderboard (
	Player Name VARCHAR(255)
	FOREIGN KEY(player_name)
	REFERENCES player(id),

	Player Game VARCHAR(255)
	FOREIGN KEY(game_name)
	REFERENCES game(id),

	Score VARCHAR(255)
	FOREIGN KEY(score_score)
	REFERENCES score(id),

	Description VARCHAR(255)
);

SELECT from leaderboard ORDER BY ASC;

